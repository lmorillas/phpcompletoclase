<?php

/*
Error reporting.
*/
ini_set("error_reporting", "true");
error_reporting(E_ALL);


function config_twig(){
	require_once realpath(dirname(__FILE__) . '/../vendor/twig/twig/lib/Twig/Autoloader.php');
	Twig_Autoloader::register();
	
	$loader = new Twig_Loader_Filesystem(realpath(dirname(__FILE__) . '/../views/'));
	
	$twig = new Twig_Environment($loader); //,  array('cache' => '/tmp/cachetwig'	,));
	
	$escaper = new Twig_Extension_Escaper(true);
	$twig->addExtension($escaper);
		
	$twig->getExtension('core')->setTimezone('Europe/Madrid');
	return $twig;
}

$passwd_adm = '$2y$10$q2FCmR5uyrCCqjlvvt/P4eU/Zo4hpOunWlxY0C3tMl22C6SZ.yvhu';

// Base de datos

$database = 'sqlite:database.db';

	?>