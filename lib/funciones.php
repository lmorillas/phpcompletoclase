<?php

function cifra_passwd($passwd){
	if (function_exists("password_hash")){
		return password_hash($passwd, PASSWORD_DEFAULT);
	}
	else{
		return crypt($passwd);
	}
}


function comprueba_passwd($passwd, $cifrada){
	if (function_exists("password_hash")){
		return password_verify($passwd, $cifrada);
	}
	else{
		return crypt($passwd, $cifrada) == $cifrada;
	}
}

// echo cifra_passwd("root");
// $test = '$2y$10$q2FCmR5uyrCCqjlvvt/P4eU/Zo4hpOunWlxY0C3tMl22C6SZ.yvhu';
// echo '<hr>', comprueba_passwd('root', $test);


function clean($val) {
	return htmlentities(strip_tags(trim($val)), ENT_QUOTES, 'UTF-8');
}

function logout(){
	unset($_SESSION['user']);
	session_destroy();
	}


?>
