<?php
//include_once "lib/config.php";


function conectar($database){
	try {
	    $conn = new PDO($database);
	    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $conn;
	    
	}
	catch(PDOException $e) {
	    echo 'ERROR: ' . $e->getMessage();
		return false;
	}
}



?>